﻿using System;
using System.Collections.Generic;

namespace lv4
{
    class Program
    {
        static void Main(string[] args)
        {

            //Z2.
           
            Dataset dataset = new Dataset(@"C:\Users\Laura\Desktop\CSV.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] rowAverage = adapter.CalculateAveragePerRow(dataset);
            double[] columnAverage = adapter.CalculateAveragePerColumn(dataset);
            
            for(int i = 0; i < rowAverage.Length; i++)
            {
                Console.Write(rowAverage[i] + " ");   
            }
            Console.WriteLine();

            for(int j = 0; j < columnAverage.Length; j++)
            {
                Console.Write(columnAverage[j] + " ");
            }

            Console.WriteLine("\n");


            //Z3.

            List<IRentable> rentables = new List<IRentable>();
            Book book = new Book("Fantasy book");
            rentables.Add(book);
            Video video = new Video("Funny film");
            rentables.Add(video);
            RentingConsolePrinter renting = new RentingConsolePrinter();
            renting.PrintTotalPrice(rentables);
            renting.DisplayItems(rentables);

            Console.WriteLine("\n");


            //Z4.

            Book book1 = new Book("Horror book");
            Video video1 = new Video("Action film");
            HotItem hotItem = new HotItem(book1);
            rentables.Add(hotItem);
            HotItem hotItem1 = new HotItem(video1);
            rentables.Add(hotItem1);
            renting.PrintTotalPrice(rentables);
            renting.DisplayItems(rentables);

            Console.WriteLine("\n");

            //koristenjem dekoratera(HotItem) ispred imena knjige i videa dodaje se opis i povecava se cijena za 1.99,
            // a sto se tice pozivanja metoda za ispis sve ostaje isto

        }
    }
}
