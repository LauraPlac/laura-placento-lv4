﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv4
{

    //Z3.
    class Book : IRentable
    {
        public String Name { get; private set; }
        private readonly double BaseBookPrice = 3.99;


        public string Description { get { return this.Name; } }


        public Book(String name)
        {
             this.Name = name; 
        }

        public double CalculatePrice() { return BaseBookPrice; }
    }
}
